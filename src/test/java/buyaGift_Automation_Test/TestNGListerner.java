package buyaGift_Automation_Test;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
public class TestNGListerner implements ITestListener{

	public void onFinish(ITestContext result) {
		
		
	}

	public void onStart(ITestContext result) {
		
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		
		
	}

	public void onTestFailure(ITestResult result) {
		
		System.out.println("TestCases failed and details are "+result.getName());
		
		
	}

	public void onTestSkipped(ITestResult result) {
		
		System.out.println("TestCases Skipped and details are "+result.getName());
		
		
	}

	public void onTestStart(ITestResult result) {
		
		System.out.println("TestCases started and details are "+result.getName());
		
		
	}

	public void onTestSuccess(ITestResult result) {
		
		System.out.println("TestCases Success Well Done! and details are "+result.getName());
		
		
	}

}
